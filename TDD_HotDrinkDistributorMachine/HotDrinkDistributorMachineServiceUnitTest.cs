using HotDrinkDistributorMachine.Enumerators;
using HotDrinkDistributorMachine.Models;
using HotDrinkDistributorMachine.Services.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TDD_HotDrinkDistributorMachine
{
    [TestClass]
    public class HotDrinkDistributorMachineServiceUnitTest
    {
        double _delta = 0.001;
        HotDrinkDistributorMachineService _hotDrinkDistributorMachineService = new HotDrinkDistributorMachineService();
        [TestMethod]
        public void GetPriceResultTestMethod()
        {
            PriceResult priceResult = _hotDrinkDistributorMachineService.GetPriceResult(RecipeEnum.Espresso);
            Assert.AreEqual(priceResult.SellingPrice, 1.56, _delta);

            priceResult = _hotDrinkDistributorMachineService.GetPriceResult(RecipeEnum.Elongate);
            Assert.AreEqual(priceResult.SellingPrice, 1.82, _delta);

            priceResult = _hotDrinkDistributorMachineService.GetPriceResult(RecipeEnum.Capuccino);
            Assert.AreEqual(priceResult.SellingPrice, 3.51, _delta);

            priceResult = _hotDrinkDistributorMachineService.GetPriceResult(RecipeEnum.Chocolate);
            Assert.AreEqual(priceResult.SellingPrice, 5.33, _delta);

            priceResult = _hotDrinkDistributorMachineService.GetPriceResult(RecipeEnum.Tea);
            Assert.AreEqual(priceResult.SellingPrice, 3.12, _delta);

        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "Using undifined Recipe throw ArgumentNullException")]
        public void CheckNullArgumentExceptionThrownWhenUndifinedArgumentTestMethod()
        {
            PriceResult priceResult = _hotDrinkDistributorMachineService.GetPriceResult((RecipeEnum)10);
        }
    }
}
