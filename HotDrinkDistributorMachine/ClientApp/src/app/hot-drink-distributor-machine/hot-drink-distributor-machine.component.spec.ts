import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotDrinkDistributorMachineComponent } from './hot-drink-distributor-machine.component';

describe('HotDrinkDistributorMachineComponent', () => {
  let component: HotDrinkDistributorMachineComponent;
  let fixture: ComponentFixture<HotDrinkDistributorMachineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotDrinkDistributorMachineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotDrinkDistributorMachineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
