import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-hot-drink-distributor-machine',
  templateUrl: './hot-drink-distributor-machine.component.html',
  styleUrls: ['./hot-drink-distributor-machine.component.css']
})
export class HotDrinkDistributorMachineComponent implements OnInit {

  public priceResult: PriceResult;
  public recipeEnum = RecipeEnum;
  http: HttpClient;
  baseUrl: string;
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.http = http;
    this.baseUrl = baseUrl;
  }

  ngOnInit() {
  }
  getPriceResult(recipe: RecipeEnum) {
    this.http.get<PriceResult>(this.baseUrl + 'HotDrinkDistributorMachine?recipe=' + recipe).subscribe(result => {
      this.priceResult = result;
    }, error => {
      console.error(error);
    });
  }

}

interface PriceResult {
  ingredients: string;
  sellingPrice: number;
}
enum RecipeEnum {
  Espresso = 1,
  Elongate = 2,
  Capuccino = 3,
  Chocolate = 4,
  Tea = 5

}

