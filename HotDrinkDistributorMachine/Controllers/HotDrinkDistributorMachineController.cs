﻿using HotDrinkDistributorMachine.Enumerators;
using HotDrinkDistributorMachine.Models;
using HotDrinkDistributorMachine.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotDrinkDistributorMachine.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HotDrinkDistributorMachineController : Controller
    {
        private readonly ILogger<HotDrinkDistributorMachineController> _logger;
        private readonly IHotDrinkDistributorMachineService _hotDrinkDistributorMachineService;

        public HotDrinkDistributorMachineController(ILogger<HotDrinkDistributorMachineController> logger,
            IHotDrinkDistributorMachineService hotDrinkDistributorMachineService)
        {
            _logger = logger;
            _hotDrinkDistributorMachineService = hotDrinkDistributorMachineService;
        }

        [HttpGet]
        public PriceResult Get(RecipeEnum recipe)
        {
            return _hotDrinkDistributorMachineService.GetPriceResult(recipe);
        }
    }
}
