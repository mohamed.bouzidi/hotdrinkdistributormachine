﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotDrinkDistributorMachine.Models
{
    public class PriceResult
    {
        public string Ingredients { get; set; }
        public double SellingPrice { get; set; }
    }
}
