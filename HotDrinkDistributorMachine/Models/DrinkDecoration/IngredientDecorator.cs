﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotDrinkDistributorMachine.Models.DrinkDecoration
{
    public abstract class IngredientDecorator: IDrink
    {
        IDrink _drink;

        protected string _name = "Ingrédient non défini";
        protected double _price = 0.0;
        public static readonly double _sellingMarge = 0.3;

        public IngredientDecorator(IDrink drink)
        {
            _drink = drink;
        }

        public string GetDescription()
        {
            return string.Format("{0}, {1}", _drink.GetDescription(), _name);
        }

        public double GetCost()
        {
            return _drink.GetCost() + _price;
        }
    }
}
