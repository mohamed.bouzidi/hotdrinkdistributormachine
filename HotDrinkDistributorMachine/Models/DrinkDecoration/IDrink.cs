﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotDrinkDistributorMachine.Models.DrinkDecoration
{
    public interface IDrink
    {
        string GetDescription();
        double GetCost();
    }
}
