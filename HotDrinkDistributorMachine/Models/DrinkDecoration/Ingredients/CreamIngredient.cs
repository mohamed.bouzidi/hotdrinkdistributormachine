﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotDrinkDistributorMachine.Models.DrinkDecoration.Ingredients
{
    public class CreamIngredient : IngredientDecorator
    {
        public CreamIngredient(IDrink drink) : base(drink)
        {
            _name = "Crème";
            _price = 0.5;
        }
    }
}
