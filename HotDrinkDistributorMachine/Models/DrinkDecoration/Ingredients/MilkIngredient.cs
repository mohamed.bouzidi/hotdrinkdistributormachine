﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotDrinkDistributorMachine.Models.DrinkDecoration.Ingredients
{
    public class MilkIngredient : IngredientDecorator
    {
        public MilkIngredient(IDrink drink) : base(drink)
        {
            _name = "Lait";
            _price = 0.4;
        }
    }
}