﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotDrinkDistributorMachine.Models.DrinkDecoration.Ingredients
{
    public class WaterIngredient : IngredientDecorator
    {
        public WaterIngredient(IDrink drink) : base(drink)
        {
            _name = "Eau";
            _price = 0.2;
        }
    }
}