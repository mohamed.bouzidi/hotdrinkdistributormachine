﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotDrinkDistributorMachine.Models.DrinkDecoration.Ingredients
{
    public class SugarIngredient : IngredientDecorator
    {
        public SugarIngredient(IDrink drink) : base(drink)
        {
            _name = "Sucre";
            _price = 0.1;
        }
    }
}
