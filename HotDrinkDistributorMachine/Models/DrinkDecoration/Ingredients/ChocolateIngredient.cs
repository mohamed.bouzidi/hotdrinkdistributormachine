﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotDrinkDistributorMachine.Models.DrinkDecoration.Ingredients
{
    public class ChocolateIngredient : IngredientDecorator
    {
        public ChocolateIngredient(IDrink drink) : base(drink)
        {
            _name = "Chocolat";
            _price = 1;
        }
    }
}