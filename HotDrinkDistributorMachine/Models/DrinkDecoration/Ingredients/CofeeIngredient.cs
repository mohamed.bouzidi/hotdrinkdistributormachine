﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotDrinkDistributorMachine.Models.DrinkDecoration.Ingredients
{
    public class CofeeIngredient : IngredientDecorator
    {
        public CofeeIngredient(IDrink drink) : base(drink)
        {
            _name = "Café";
            _price = 1;
        }
    }
}
