﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotDrinkDistributorMachine.Models.DrinkDecoration.Ingredients
{
    public class TeaIngredient : IngredientDecorator
    {
        public TeaIngredient(IDrink drink) : base(drink)
        {
            _name = "Thé";
            _price = 2;
        }
    }
}