﻿using HotDrinkDistributorMachine.Models.DrinkDecoration;
using HotDrinkDistributorMachine.Models.DrinkDecoration.Ingredients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotDrinkDistributorMachine.Models.Recipe
{
    public class Tea : IDrink
    {
        public double GetCost()
        {
            return 0;
        }

        public string GetDescription()
        {
            return "Recette Thé";
        }

        public IDrink GetRecipe()
        {
            return new WaterIngredient(new WaterIngredient(new TeaIngredient(new Tea())));
        }
    }
}
