﻿using HotDrinkDistributorMachine.Models.DrinkDecoration;
using HotDrinkDistributorMachine.Models.DrinkDecoration.Ingredients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotDrinkDistributorMachine.Models.Recipe
{
    public class Capuccino : IDrink
    {
        public double GetCost()
        {
            return 0;
        }

        public string GetDescription()
        {
            return "Recette Capuccino";
        }

        public IDrink GetRecipe()
        {
            return new CreamIngredient(new WaterIngredient(new ChocolateIngredient(new CofeeIngredient(new Capuccino()))));
        }
    }
}