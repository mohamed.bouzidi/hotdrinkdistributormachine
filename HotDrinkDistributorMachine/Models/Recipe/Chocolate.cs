﻿using HotDrinkDistributorMachine.Models.DrinkDecoration;
using HotDrinkDistributorMachine.Models.DrinkDecoration.Ingredients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotDrinkDistributorMachine.Models.Recipe
{
    public class Chocolate : IDrink
    {
        public double GetCost()
        {
            return 0;
        }

        public string GetDescription()
        {
            return "Recette Chocolat";
        }

        public IDrink GetRecipe()
        {
            return new SugarIngredient(new WaterIngredient
                (new MilkIngredient(new MilkIngredient
                (new ChocolateIngredient(new ChocolateIngredient(new ChocolateIngredient
                (new Chocolate())))))));
        }
    }
}
