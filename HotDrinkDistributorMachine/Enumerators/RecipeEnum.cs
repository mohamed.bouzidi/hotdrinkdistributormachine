﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotDrinkDistributorMachine.Enumerators
{
    public enum RecipeEnum
    {
        Espresso = 1,
        Elongate = 2,
        Capuccino = 3,
        Chocolate = 4,
        Tea = 5

    }
}
