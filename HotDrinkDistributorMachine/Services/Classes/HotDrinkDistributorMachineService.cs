﻿using HotDrinkDistributorMachine.Enumerators;
using HotDrinkDistributorMachine.Models;
using HotDrinkDistributorMachine.Models.DrinkDecoration;
using HotDrinkDistributorMachine.Models.Recipe;
using HotDrinkDistributorMachine.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotDrinkDistributorMachine.Services.Classes
{
    public class HotDrinkDistributorMachineService : IHotDrinkDistributorMachineService
    {
        public PriceResult GetPriceResult(RecipeEnum recipe)
        {
            
            IDrink myDrink = GetMyDrink(recipe);

            if(myDrink == null)
            {
                throw new ArgumentNullException ();
            }

            return new PriceResult
            {
                Ingredients = myDrink.GetDescription(),
                SellingPrice = myDrink.GetCost() + (myDrink.GetCost() * IngredientDecorator._sellingMarge)
            };
        }

        private IDrink GetMyDrink(RecipeEnum recipe)
        {
            switch (recipe)
            {
                case RecipeEnum.Espresso:
                    return new Espresso().GetRecipe();
                case RecipeEnum.Elongate:
                    return new Elongate().GetRecipe();
                case RecipeEnum.Capuccino:
                    return new Capuccino().GetRecipe();
                case RecipeEnum.Chocolate:
                    return new Chocolate().GetRecipe();
                case RecipeEnum.Tea:
                    return new Tea().GetRecipe();
            }
            return null;
        }
    }
}
