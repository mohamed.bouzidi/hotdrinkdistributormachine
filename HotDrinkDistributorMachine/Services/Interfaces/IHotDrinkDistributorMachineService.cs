﻿using HotDrinkDistributorMachine.Enumerators;
using HotDrinkDistributorMachine.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotDrinkDistributorMachine.Services.Interfaces
{
    public interface IHotDrinkDistributorMachineService
    {
        PriceResult GetPriceResult(RecipeEnum recipe);
    }
}
