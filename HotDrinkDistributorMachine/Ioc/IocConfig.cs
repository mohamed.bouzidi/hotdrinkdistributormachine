﻿using HotDrinkDistributorMachine.Services.Classes;
using HotDrinkDistributorMachine.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace HotDrinkDistributorMachine.Ioc
{
    public class IocConfig
    {
        public static void AddServicesInjections(IServiceCollection services)
        {
            services.AddScoped<IHotDrinkDistributorMachineService, HotDrinkDistributorMachineService>();
        }
    }
}
